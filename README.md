This repository contains my CV, in LaTeX.

If you want to see the "compiled" version in PDF, you can browse in
this repository through "Pipelines" and see the latest "green build".

I'm currently working on create a web based version. Until I create a
suitable one, please feel free to ask me for the PDF version if you
are interested,
