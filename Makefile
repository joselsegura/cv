# -*- mode: Makefile; coding: utf-8 -*-

.PHONY: clean-all clean

%.pdf: %.tex
	rubber --pdf $^

clean:
	$(RM) *.aux *.log *.out

clean-all: clean
	$(RM) *.pdf
